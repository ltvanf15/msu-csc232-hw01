/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file   Bag.h
 * @authors Frank M. Carrano
 *          Timothy M. Henry
 * @brief  Bag interface specification.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef HW01_BAG_H
#define HW01_BAG_H

#include <vector>

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {

    /**
     * @brief Bag Interface introduced in Chapter 1 in Listing 1-1 with
     * modifications by Jim Daehn.
     * @tparam T class template parameter; the type of element stored in this
     * `Bag`.
     */
    template <typename T>
    class Bag {
    public:
        /**
         * @brief Gets the current number of entries in this bag.
         * @return The integer number of entries currently in this `Bag` is
         * returned.
         */
        virtual int getCurrentSize() const = 0;

        /**
         * @brief Determine whether this `Bag` is empty.
         * @return  True if this `Bag` is empty; false otherwise.
         */
        virtual bool isEmpty() const = 0;

        /**
         * @brief Adds a new entry to this `Bag`.
         * @post If successful, `newEntry` is stored in the bag and the count
         * of items in the bag has increased by 1.
         * @param newEntry The object to be added as a new entry.
         * @return True if addition was successful, or false if not
         */
        virtual bool add(const T& newEntry) = 0;

        /**
         * @brief Removes one occurrence of a given entry from this `Bag` if
         * possible.
         * @post If successful, `anEntry` has been removed from the bag and
         * the count of items i the bag has decreased by 1.
         * @param anEntry The entry to be removed.
         * @return True if removal was successful, or false if not.
         */
        virtual bool remove(const T& anEntry) = 0;

        /**
         * @brief Removes all entries from this bag.
         * @post Bag contains no items, and the count of items is 0.
         */
        virtual void clear() = 0;

        /**
         * @brief Counts the number of times a given entry appears in this bag.
         * @param anEntry The entry to be counted.
         * @return The number of times `anEntry` appears in the bag.
         */
        virtual int getFrequencyOf(const T& anEntry) const = 0;

        /**
         * @brief Tests whether this bag contains a given entry.
         * @param anEntry The entry to locate.
         * @return True if bag contains `anEntry`, or false otherwise.
         */
        virtual bool contains(const T& anEntry) const = 0;

        /**
         * @brief Empties and then fills a given `std::vector` with all
         * entries that are in this `Bag`.
         * @return A `std::vector` containing copies of all the entries in
         * this `Bag`.
         */
        virtual std::vector<T> toVector() const = 0;

        // TODO: Exercise 6 - provide union specification below with appropriate
        // doxygen comments appropriate comments that fully specify the
        // behavior of this method. When you have completed this step, commit
        // your changes with the following commit message:
        // CSC232-HW01 - Completed Exercise 6.

        // TODO: Exercise 7 - provide intersection specification below with
        // appropriate doxygen comments that fully specify the behavior of this
        // method. When you have completed this step, commit your changes with
        // the following commit message:
        // CSC232-HW01 - Completed Exercise 7.

        // TODO: Exercise 8 - provide difference specification below with
        // appropriate doxygen comments that fully specify the behavior of this
        // method. When you have completed this step, commit your changes with
        // the following commit message:
        // CSC232-HW01 - Completed Exercise 8.

        // TODO: Programming Problem 6 - provide inline definitions of the
        // above operations independently of the bag's implementation by
        // using only ADT bag operations. When you have completed this step,
        // commit your changes with the following commit message:
        // CSC232-HW01 - Completed Programming Problem 6.

        /**
         * @brief Destroys this bag and frees its assigned memory.
         */
        virtual ~Bag() {
            // inlined, no-op
        }
    }; // end Bag
} // end namespace csc232

#endif //HW01_BAG_H
